"""
.. module:: riak.py

"""

from riakasaurus import riak_object, mapreduce, bucket, client, riak_link

RiakClient = client.RiakClient
RiakBucket = bucket.RiakBucket
RiakObject = riak_object.RiakObject
RiakMapReduce = mapreduce.RiakMapReduce
RiakLink = riak_link.RiakLink
